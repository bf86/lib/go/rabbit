// Rabbit is a thin convenience wrapper around the github.com/rabbitmq/amqp091-go library
package rabbit

import (
	"context"
	"errors"
	"fmt"
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

var ctx = context.Background()

type Rabbit struct {
	Host       string
	Port       string
	connection *amqp.Connection
	channel    *amqp.Channel
	queue      amqp.Queue
}

func New(host, port string) *Rabbit {
	var envHost, envPort string
	if env, ok := os.LookupEnv("RABBIT_HOST"); ok {
		envHost = env
	}
	if env, ok := os.LookupEnv("RABBIT_PORT"); ok {
		envPort = env
	}

	if host != "" {
		envHost = host
	}
	if port != "" {
		envPort = port
	}

	return &Rabbit{
		Host: envHost,
		Port: envPort,
	}
}

func (r *Rabbit) Connect() error {
	var err error
	r.connection, err = amqp.Dial(fmt.Sprintf("amqp://%s:%s", r.Host, r.Port))
	return err
}

func (r *Rabbit) Queue(name string) error {
	var err error
	if r.connection == nil {
		return errors.New("no connection setup yet")
	}

	if r.channel == nil {
		r.channel, err = r.connection.Channel()
		if err != nil {
			return err
		}
	}

	r.queue, err = r.channel.QueueDeclare(
		name, false, false, false, false, nil,
	)
	return err
}

func (r *Rabbit) Publish(message []byte) error {
	return r.channel.PublishWithContext(
		ctx,
		"",
		r.queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        message,
		},
	)
}

func (r *Rabbit) Consume() (<-chan amqp.Delivery, error) {
	return r.channel.Consume(
		r.queue.Name, // queue
		"",           // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
}

func (r *Rabbit) Close() {
	r.channel.Close()
	r.connection.Close()
}
